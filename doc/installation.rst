==============
 Installation
==============

ivy-python runs with python 2.7 and 3.x.

PyPI
----
You can install ivy-python from PyPI with::

   pip install ivy-python

Installing from sources
-----------------------

ivy-python is developped on `gitlab <https://gitlab.com/ivybus/ivy-python>`_.

To clone the repository:

  git clone https://gitlab.com/ivybus/ivy-python.git

After you get the source, you can install the package in your python environment::

  cd ivy-python
  pip install --editable .
