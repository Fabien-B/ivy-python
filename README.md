Ivy: a lightweight software bus
===============================

[Ivy](http://www.eei.cena.fr/products/ivy/) is a lightweight
software bus for quick-prototyping protocols. It allows
applications to broadcast information through text messages, with
a subscription mechanism based on regular expressions.

This is the python implementation.  A lot of materials, including
libraries for many languages are available from the [Ivy Home
Page](http://www.eei.cena.fr/products/ivy/).

Supported versions of python are: 2.7+, 3.x

Documentation
-------------

Documentation is available at https://ivy-python.readthedocs.io/.

Upgrading from v1.x
-------------------

v2.0 adds an additional argument to callbacks. If you're upgrading
from ivy-python 1.x, be sure to read the v2.0 compatibility notes in
the [online
documentation](https://ivy-python.readthedocs.io/en/latest/ivy.html#v2-0-compatibility-notes)).


Contributors
------------

The following people have participated to the project, by reporting
bugs, proposing new features and/or proposing patches, proposing the
hosting svn & web servers (before we switched to gitlab), etc.

By chronological  order:

- Frédéric Cadier
- Marcellin Buisson
- Yannick Jestin
- Olivier Fourdan
- Olivier Saal
- Gwenael Bothorel
- Jiri Borovec
- Nicolas Courtel
- Fabien André
- Bohdan Mushkevych
- Antoine Drouin
- Felix Ruess
- Aurélien Pardon
- Duminda Ratnayake

Thank you!
